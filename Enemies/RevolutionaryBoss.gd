extends "res://Enemies/Enemy.gd"

export (int) var ACCELERATION = 100
export (int) var SPEED = 300
export (int) var GRAVITY = 200

onready var AttackRay = $AttackRay
onready var AttackTimer = $AttackTimer
onready var StatusTimer = $StatusTimer
onready var SenseRay = $SenseRay
onready var BackRay = $BackRay

enum {ATTACK, MOVE, PATROL}

var status = PATROL

# warning-ignore:unused_argument
func _physics_process(delta):
	apply_gravity(delta)
	if AttackRay.is_colliding() and AttackRay.get_collider().name == "KingsMan":
		status = ATTACK
	elif SenseRay.is_colliding() and SenseRay.get_collider().name == "KingsMan":
		status = MOVE
	elif BackRay.is_colliding() and BackRay.get_collider().name == "KingsMan":
		var turn = rng.randi_range(1, 10)
		if turn == 1:
			self.scale.x *= -1
	else:
		status = PATROL
	
	match status:
		ATTACK:
			if AttackTimer.time_left == 0:
				AttackTimer.start()
				AnimationPlayer.play("Attack")
		MOVE:
			AnimationPlayer.play("Move")
			var moveVector = global_position - SenseRay.get_collider().global_position
			var direction = sign(moveVector.x)
			var movex = speed * direction * -1
			motion = Vector2(movex, 0)
			motion = move_and_slide(motion)
		PATROL:
			AnimationPlayer.play("Move")
			if StatusTimer.time_left == 0:
				StatusTimer.start()
				var turn = rng.randi_range(1, 3)
				if turn == 1:
					self.scale.x *= -1
				var movex = self.scale.x * rng.randi_range(1, 10)
				motion = Vector2(movex, 0)
				motion = move_and_slide(motion, Vector2.UP)


func apply_gravity(delta):
	if not is_on_floor():
		motion.y += GRAVITY * delta
