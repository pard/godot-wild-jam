extends KinematicBody2D

const EnemyDeathEffect = preload("res://Effects/EnemyDeathEffect.tscn")

# warning-ignore:unused_class_variable
export(int) var MAX_SPEED = 15
# warning-ignore:unused_class_variable
var motion = Vector2.ZERO
var knockback = Vector2.ZERO
var rng = RandomNumberGenerator.new()
var speed = 30

onready var stats = $EnemyStats
onready var healthBar = $HealthBar
onready var AnimationPlayer = $AnimationPlayer

func _ready():
	rng.randomize()
	speed = rng.randi_range(30, 40)
	healthBar.set_max(stats.max_health)
	healthBar.set_value(stats.health)

func _on_Hurtbox_hit(damage):
	stats.health -= damage
	if stats.health <= 0:
		_on_EnemyStats_enemy_died()
	healthBar.set_value(stats.health)

func _on_EnemyStats_enemy_died():
	Utils.instance_scene_on_main(EnemyDeathEffect, global_position)
	queue_free()

