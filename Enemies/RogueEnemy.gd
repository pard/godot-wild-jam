extends "res://Enemies/Enemy.gd"

onready var AttackRay = $AttackRay
onready var AttackTimer = $AttackTimer
onready var StatusTimer = $StatusTimer
onready var SenseRay = $SenseRay

enum {ATTACK, MOVE, IDLE}

var status = IDLE

# warning-ignore:unused_argument
func _physics_process(delta):

	if AttackRay.is_colliding() and AttackRay.get_collider().name == "KingsMan":
		status = ATTACK
	elif SenseRay.is_colliding() and SenseRay.get_collider().name == "KingsMan":
		status = MOVE
	else:
		status = IDLE
	
	match status:
		ATTACK:
			if AttackTimer.time_left == 0:
				AttackTimer.start()
				AnimationPlayer.play("Attack")
		MOVE:
			var moveVector = global_position - SenseRay.get_collider().global_position
			var direction = sign(moveVector.x)
			speed = rng.randi_range(30, 60)
			var movex = speed * direction * -1
			motion = Vector2(movex, 0)
			motion = move_and_slide(motion)
			if not SenseRay.is_colliding():
				status = IDLE
		IDLE:
			if StatusTimer.time_left == 0:
				StatusTimer.start()
				var turn = rng.randi_range(1, 3)
				if turn == 1:
					self.scale.x *= -1
				var movex = self.scale.x * rng.randi_range(1, 10)
				motion = Vector2(movex, 0)
				motion = move_and_slide(motion)

# warning-ignore:unused_argument
func _on_TurnAround_body_entered(body):
	self.scale.x *= -1
