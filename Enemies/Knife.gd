extends "res://CollisionBoxes/Hitbox.gd"

func KnifeAttack(hurtbox):
	var player = hurtbox.get_parent()
	if player.name == "KingsMan":
		if player.ShieldTimer.time_left == 0:
			KingsmanKnockback(hurtbox.get_parent())
			return
	hurtbox.emit_signal("hit", damage)

func KingsmanKnockback(kingsMan):
	kingsMan.ShieldAnimator.play("Shove")
	kingsMan.ShieldTimer.start(kingsMan.ShieldTime)
#	self.get_parent().knockback.x += sign(kingsMan.motion.x) * kingsMan.ShieldKnockback

func _on_Knife1_area_entered(area):
	KnifeAttack(area)

func _on_Knife2_area_entered(area):
	KnifeAttack(area)
