extends "res://Enemies/Enemy.gd"

onready var SwingTimer = $SwingTimer

func _on_SwingTimer_timeout():
	var wait = rng.randf_range(1.0,3.0)
	yield(get_tree().create_timer(wait), "timeout")
	var side = rng.randi_range(0,1)
	if side == 1:
		AnimationPlayer.play("Left")
	else:
		AnimationPlayer.play("Right")
	
	SwingTimer.start()
