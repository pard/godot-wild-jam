extends Area2D

var damage = 1

func _on_Sword_area_entered(hurtbox):
	var player = hurtbox.get_parent()
	if player.name == "KingsMan":
		if player.ShieldTimer.time_left == 0:
			player.ShieldAnimator.play("Shove")
			player.ShieldTimer.start(player.ShieldTime)
			return
			
	hurtbox.emit_signal("hit", damage)
