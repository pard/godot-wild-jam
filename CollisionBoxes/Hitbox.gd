extends Area2D

const ExplosionEffect = preload("res://Effects/ExplosionEffect.tscn")

export(int) var damage = 1

func _on_Hitbox_area_entered(hurtbox):
	hurtbox.emit_signal("hit", damage)
	var enemy = hurtbox.get_parent()
	print(enemy)
	var kingsMan = self.get_parent()
	if (enemy.name != "KingsMan"):
		enemy.knockback.x += sign(kingsMan.motion.x) * kingsMan.ShieldKnockback
	
	Utils.instance_scene_on_main(ExplosionEffect, enemy.global_position)
