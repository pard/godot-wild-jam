extends Area2D

onready var DoorTimer = $DoorTimer

# warning-ignore-all:unused_class_variable
export(Resource) var connection = null
var _active = true

func _on_Door_body_entered(Player):
	if _active == true:
		DoorTimer.start()
		_active = false
		Player.emit_signal("hit_door", self)

func _on_DoorTimer_timeout():
	_active = true
