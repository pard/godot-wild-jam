extends Node

var PlayerStats = ResourceLoader.PlayerStats

onready var healthBar = $HealthBar

export(int) var max_amount = 4 setget set_max
export(int) var amount = 4 setget set_value

func _ready():
	healthBar.max_value = max_amount
	healthBar.value = amount
	if (get_parent().name == "Rogue" or get_parent().name == "KingsMan"):
		PlayerStats.connect("player_health_changed", self, "set_value")

func set_max(new_max):
	max_amount = max(1, new_max)
	healthBar.max_value = max_amount
	
func set_value(new_value):
	amount = clamp(new_value, 0, max_amount)
	healthBar.value = amount


