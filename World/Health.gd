extends Node

signal max_changed(new_max)
signal changed(new_amount)

export(int) var max_amount = 10 setget set_max
onready var current = max_amount setget set_current

func _ready():
	_initialize()

func set_max(new_max_value):
	max_amount = max(1, new_max_value)
	emit_signal("max_changed", max_amount)
	
func set_current(new_value):
	current = clamp(current, 0, new_value)
	emit_signal("changed", current)

func _initialize():
	emit_signal("max_changed", max_amount)
	emit_signal("changed", current)
