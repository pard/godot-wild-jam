extends Node2D

var rng = RandomNumberGenerator.new()
var PlayerStats = ResourceLoader.PlayerStats

onready var AnimationPlayer = $AnimationPlayer

# warning-ignore:unused_argument
func _on_Area2D_body_entered(body):
	if PlayerStats.health < PlayerStats.max_health:
		rng.randomize()
		PlayerStats.max_health += 1
		var energyAmount = rng.randi_range(3, 6)
		PlayerStats.health += energyAmount
		AnimationPlayer.play("Drink")

