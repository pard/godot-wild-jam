extends "res://Player/Player.gd"

export(int) var ShieldKnockback = 200
export(float) var ShieldTime = 2

onready var ShieldTimer = $Shield/ShieldTimer
onready var ShieldAnimator = $ShieldAnimator
onready var SpriteAnimator = $SpriteAnimation
onready var Shield = $Shield
onready var Sword = $Sword

func update_animations(input_vector):
	.update_animations(input_vector)
	var facing = sign(input_vector.x)
	if facing != 0:
		Shield.scale.x = facing
		Sword.scale.x = facing
	if input_vector == Vector2.ZERO:
		SpriteAnimator.play("Idle")
	else:
		SpriteAnimator.play("Walk")
func move():
	var was_in_air = not is_on_floor()
	var was_on_floor = is_on_floor()
	var last_motion = motion
	var last_position = position
	motion = move_and_slide_with_snap(motion, snap_vector*4, Vector2.UP, true, 4, deg2rad(MAX_SLOPE_ANGLE))
	# Landing
	if was_in_air and is_on_floor():
		motion.x = last_motion.x
		Utils.instance_scene_on_main(JumpEffect, global_position)
		double_jump = false
	
	# Just left ground
	if was_on_floor and not is_on_floor() and not just_jumped:
		SpriteAnimator.play("Walk", -1, 1, true)
		motion.y = 0
		position.y = last_position.y
		coyoteJumpTimer.start()
	
	# Prevent Sliding (hack)
	if is_on_floor() and get_floor_velocity().length() == 0 and abs(motion.x) < 1:
		position.x = last_position.x


func _on_ShieldTimer_timeout():
	ShieldAnimator.stop()
	ShieldAnimator.play("Reset")


func _on_ShieldAnimator_animation_finished(anim_name):
	if anim_name == "Shove":
		ShieldAnimator.play("Blink")


func _on_AudioStreamPlayer_finished():
	$Music.play(0.30)
